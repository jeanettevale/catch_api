<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesMytopiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages_mytopia', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('message_id');
            $table->longText('body')->nullable();
            $table->string('commercial_id',45)->nullable();
            $table->dateTime('date_created')->nullable();
            $table->dateTime('doc_date_uploaded')->nullable();
            $table->string('doc_file_name',255)->nullable();
            $table->integer('doc_fle_size')->nullable();
            $table->integer('doc_id')->nullable();
            $table->string('doc_type',45)->nullable();
            $table->string('from_id',45)->nullable();
            $table->string('from_name',255)->nullable();
            $table->string('from_type',45)->nullable();
            $table->integer('offer_id')->nullable();
            $table->string('order_id',45)->nullable();
            $table->string('subject',255)->nullable();
            $table->integer('to_customer_id')->nullable();
            $table->string('to_customer_name',255)->nullable();
            $table->tinyInteger('to_operator')->nullable();
            $table->integer('to_shop_id')->nullable();
            $table->string('to_shop_name',255)->nullable();
            $table->tinyInteger('visible')->nullable();
            $table->dateTime('sent_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages_mytopia');
    }
}
