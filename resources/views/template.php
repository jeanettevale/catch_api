<!doctype html>
<html>
<head>
    <style type="text/css">
        .header_edisons{
            background: black;
            padding:0px;
            text-align: center;
        }
        .header_mytopia{
            background: #dad8d8;
            padding: 20px;
            text-align: center;
        }
        .msgWrapper{
            padding-top: 30px;
        }
        .tblForm{
            width: 100%;
        }
        .tblForm td, .msgSignature{
            padding:20px;
            font-family: Helvetica;
            color: #47475f;
        }
        .border{
            border-bottom: 1px solid rgba(0, 0, 0, 0.1);
        }
    </style>
</head>
<body>
<div class="<?php echo $header_class; ?>">
    <img src="<?php echo $header_image; ?>" />
</div>
<div align="center" class="msgWrapper">
    <table class="tblForm" cellpadding="0" cellspacing="0">
        <col width="50%" span="2"></col>
        <tr>
            <td class="border">
                <strong>From:</strong> <?php echo (isset($from_name) ? $from_name : ""); ?>
            </td>
            <td class="border">
                <strong>Order ID:</strong> <?php echo (isset($order_id) ? $order_id : ""); ?>
            </td>
        </tr>
        <tr>
            <td class="border">
                <strong>To:</strong> <?php echo (isset($to_shop_name) ? $to_shop_name : $to_customer_name); ?>
            </td>
            <td class="border">
                <strong>Offer ID:</strong> <?php echo (isset($order_id) ? $order_id : ""); ?>
            </td>
        </tr>
        <tr>
            <td class="border"><strong>Sent:</strong> <?php echo $created; ?></td>
            <td class="border">
                <strong>Attachment:</strong>
                <a href="<?php echo $document_url ?>"><?php echo $filename; ?></a>>
            </td>
        </tr>
        <tr>
            <td colspan="2"> <?php echo $body; ?> </td>
        </tr>
    </table>
    <div class="msgSignature">
        Mills API<br/>
        <strong>Edisons</strong><br/>
        <a href="https://www.catch.com.au/">https://www.catch.com.au/</a>
    </div>
</div>
</body>
</html>