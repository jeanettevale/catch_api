<!doctype html>
<html>
<head>
    <style type="text/css">
        .header{
            background: #dad8d8;
            padding: 20px;
            text-align: center;
        }
        .msgWrapper{
            padding-top: 30px;
        }
        .tblForm{
            width: 100%;
        }
        .tblForm td, .msgSignature{
            padding:20px;
            font-family: Helvetica;
            color: #47475f;
        }
        .border{
            border-bottom: 1px solid rgba(0, 0, 0, 0.1);
        }
    </style>
</head>
<body>
<div class="header">
    <img src="https://gallery.mailchimp.com/d4199be1654d677b729cc26d9/images/2017ce9d-5cbe-4794-82a8-eecc2e1850c1.png" />
</div>
<div align="center" class="msgWrapper">
    <table class="tblForm" cellpadding="0" cellspacing="0">
        <col width="50%" span="2"></col>
        <tr>
            <td class="border"><strong>From:</strong> <?php echo $from_name ?> </td>
            <td class="border"><strong>To:</strong> <?php echo (isset($msg->to_shop_name) ? $msg->to_shop_name : "") ?> </td>
        </tr>
        <tr>
            <td class="border"><strong>Sent:</strong> <?php echo $created ?> </td>
            <td class="border"><strong>Order ID:</strong> <?php echo $order_id ?> </td>
        </tr>
        <tr>
            <td colspan="2"> <?php echo $body ?> </td>
        </tr>
    </table>
    <div class="msgSignature">
        Mills API<br/>
        <strong>Mytopia</strong><br/>
        <a href="https://www.catch.com.au/">https://www.catch.com.au/</a>
    </div>
</div>
</body>
</html>