<?php
/**
 * Created by PhpStorm.
 * User: jeanette
 * Date: 19/09/2017
 * Time: 1:27 PM
 */

namespace App\Http\Controllers;

class FileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public static function downloadFile($shop, $document_id){
        $apikey = ($shop == "edisons" ? env('API_KEY_EDISONS','') : env('API_KEY_MYTOPIA',''));
        $apiUrl = env('API_URL_DOWNLOAD','');

        $ch = curl_init();

        // 2. set the options, including the url
        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: ' . $apikey,
            'Content-Type: application/json'
        ));

        // 3. execute and fetch the resulting HTML output
        $output = curl_exec($ch);

        if ($output === FALSE) {
            echo "cURL Error: " . curl_error($ch);
        }

        // 4. free up the curl handle
        curl_close($ch);
    }
}
