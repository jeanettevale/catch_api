<?php
/**
 * Created by PhpStorm.
 * User: jeanette
 * Date: 19/09/2017
 * Time: 11:17 AM
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class MessagingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getMessages($shop){
        $apikey = ($shop == "edisons" ? env('API_KEY_EDISONS','') : env('API_KEY_MYTOPIA',''));
        $apiUrl = env('API_URL','') . '?max=' . env('MAX_RET','100') . '&received=' . env('RECEIVED','ALL');

        $ch = curl_init();

        // 2. set the options, including the url
        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: ' . $apikey,
            'Content-Type: application/json'
        ));

        // 3. execute and fetch the resulting HTML output
        $output = curl_exec($ch);
        $o = json_decode($output,true);
        usort($o["messages"], function($a, $b){
            return strtotime($a["date_created"]) - strtotime($b["date_created"]);
        });
        $o_std = json_decode(json_encode($o));

        if ($output === FALSE) {
            echo "cURL Error: " . curl_error($ch);
        }

        // 4. free up the curl handle
        curl_close($ch);

        $this->processMessages($o_std, $shop);
    }

    public function processMessages($result, $shop){
        $messages = $result->messages;
        $table = ($shop == "edisons" ? "messages_edisons": "messages_mytopia");
//        $view = ($shop == "edisons" ? "template": "template_mytopia");

        foreach ($messages as $msg){
            $select = DB::select('SELECT COUNT(*) as total FROM ' . $table . ' WHERE message_id = ' . $msg->id)[0];
            if($select->total == 0){
                $this->sendMessage($msg, $shop, $table);
            }
        }
    }

    public function sendMessage($msg, $shop, $table){
        $tz = env('TIMEZONE');
        $subject = "[Catch] " . $msg->commercial_id . ": " . $msg->subject;

        $msg->created = Carbon::parse($msg->date_created)->setTimezone($tz)->format('m-d-Y H:i:s');
        $msg->date_today = Carbon::now()->setTimezone($tz);
        $msg->shop = $shop;
        if($shop == "edisons"){
            $msg->header_class = "header_edisons";
            $msg->header_image = "https://gallery.mailchimp.com/d8ddfd320e003f05075d19e3f/images/4e7305a3-d1a1-4ba6-8b36-bb7397cc8af7.jpg";
            if(isset($msg->document->id))
                $msg->document_url = env('SERVER_URL') . "/download/edisons/" . $msg->document->id;
        } else if ($shop == "mytopia") {
            $msg->header_class = "header_mytopia";
            $msg->header_image = "https://gallery.mailchimp.com/d4199be1654d677b729cc26d9/images/2017ce9d-5cbe-4794-82a8-eecc2e1850c1.png";
            if(isset($msg->document->id))
                $msg->document_url = env('SERVER_URL') . "/download/mytopia/" . $msg->document->id;
        }
        if(!isset($msg->document->id)) {
            $msg->document_url = "#";
            $msg->filename = "";
        }
        else $msg->filename = $msg->document->file_name . '.' . $msg->document->type;

        $arr = json_decode(json_encode($msg), true);
        try {
            Mail::send('template', $arr, function ($msg) use ($subject){
                $msg->subject($subject);
            });
            $this->insertMessage($msg, $table);
        } catch (Exception $e){
            echo $e->getMessage();
        }
    }

    public function insertMessage($msg, $table){
        DB::insert('INSERT INTO ' . $table . ' (
                            message_id,body,commercial_id,date_created,
                            doc_date_uploaded,doc_file_name,doc_fle_size,doc_id,doc_type,
                            from_id,from_name,from_type,offer_id,order_id,subject,to_customer_id,to_customer_name,
                            to_operator,to_shop_id,to_shop_name,visible,sent_date)
                    VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
            [$msg->id,
                (isset($msg->body) ? $msg->body : null),
                (isset($msg->commercial_id) ? $msg->commercial_id : null),
                (isset($msg->date_created) ? Carbon::parse($msg->date_created)->setTimezone(env('TIMEZONE')) : null),
                (isset($msg->document->date_uploaded) ? $msg->document->date_uploaded : null),
                (isset($msg->document->file_name) ? $msg->document->file_name : null),
                (isset($msg->document->file_size) ? $msg->document->file_size : null),
                (isset($msg->document->id) ? $msg->document->id : null),
                (isset($msg->document->type) ? $msg->document->type : null),
                (isset($msg->from_id) ? $msg->from_id : null),
                (isset($msg->from_name) ? $msg->from_name : null),
                (isset($msg->from_type) ? $msg->from_type : null),
                (isset($msg->offer_id) ? $msg->offer_id : null),
                (isset($msg->order_id) ? $msg->order_id : null),
                (isset($msg->subject) ? $msg->subject : "(No Subject)"),
                (isset($msg->to_customer_id) ? $msg->to_customer_id : null),
                (isset($msg->to_customer_name) ? $msg->to_customer_name : null),
                (isset($msg->to_operator) ? $msg->to_operator : false),
                (isset($msg->to_shop_id) ? $msg->to_shop_id : null),
                (isset($msg->to_shop_name) ? $msg->to_shop_name : null),
                (isset($msg->visible) ? $msg->visible : true),
                $msg->date_today]
        );
    }
}